package diogo.plugin;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.milkbowl.vault.economy.Economy;

public class Comands implements CommandExecutor, Listener {

	private static final int money = 150;

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String lb, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("�1 Apenas para Jogadores");
			return true;

		}
		Player p = (Player) sender;
		Inventory storefree = Bukkit.createInventory(null, 3 * 9,
				ChatColor.translateAlternateColorCodes('&', "&bLoja FREE"));
		Inventory storevip = Bukkit.createInventory(null, 3 * 9,
				ChatColor.translateAlternateColorCodes('&', "&bLoja VIP"));
		Inventory trash = Bukkit.createInventory(null, 3 * 9,
				ChatColor.translateAlternateColorCodes('&', "&bDeposite seu Lixo Aqui"));
		if (!p.hasPermission("comando.use")) {
			p.sendMessage("�1 Nao tem permissao");
			return true;
		}
		if (!p.hasPermission("login.use")) {
			p.sendMessage("�1 Nao tem permissao");
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("saldo")) {
			if (args.length == 0) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						"&bSeu saldo � :" + getEconomy().getBalance(p) + "$"));
			}
			if (p.isOp()) {

				if (args[0].equalsIgnoreCase("add")) {
					if (args.length == 3) {
						Player target = Bukkit.getPlayerExact(args[1]);
						if (target == null) {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&',
									"&b Jogador" + args[1] + "&bN�o existe"));
							return true;
						}
						try {

							Integer quant = new Integer(args[2]);
							getEconomy().depositPlayer(target, quant);
							p.sendMessage(ChatColor.translateAlternateColorCodes('&',
									"&b voc� depositou" + quant + "&b $ para o jogador:" + target.getName()));

						} catch (NumberFormatException e) {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b Digite uma quantia"));
						}
					}
				}
				if (args[0].equalsIgnoreCase("dell")) {
					if (args.length == 3) {
						Player target = Bukkit.getPlayerExact(args[1]);
						if (target == null) {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&',
									"&b Jogador" + args[1] + "&bN�o existe"));
							return true;
						}
						try {

							Integer quant = new Integer(args[2]);
							if (getEconomy().has(target, quant)) {
								getEconomy().withdrawPlayer(target, quant);
								return true;
							} else {
								p.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"&bO Jogador n�o possui esta quantia" + "&bA qaunti atual do jogador � de"
												+ getEconomy().getBalance(target) + "&b $"));
								return true;
							}

						} catch (NumberFormatException e) {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b Digite uma quantia"));
						}
					}
				}
			}
		}
		if (cmd.getName().equalsIgnoreCase("comando")) {
			if (args.length == 0) {
				p.sendMessage("�aVoc� executou o arg 00");
			} else if (args.length == 1 && args[0].equalsIgnoreCase("xp")) {
				p.sendTitle(p.getName(), "Seu Xp �" + p.getExp());
				p.playSound(p.getLocation(), Sound.BLOCK_BEACON_ACTIVATE, 0.5f, 0.5f);
			}
		}
		if (cmd.getName().equalsIgnoreCase("login")) {
			if (args.length == 0) {
				p.sendMessage("�aDigite sua senha");
			} else if (args.length == 1 && args[0].equalsIgnoreCase("123456")) {
				p.sendTitle(p.getName(), "Esta Logado");
				p.playSound(p.getLocation(), Sound.BLOCK_BEACON_ACTIVATE, 0.5f, 0.5f);
			}
		}

		if (cmd.getName().equalsIgnoreCase("loja")) {
			if (args.length == 0) {
				p.sendMessage("�aDigite loja VIP ou loja FREE");
			} else if (args.length == 1 && args[0].equalsIgnoreCase("FREE")) {

				ItemStack item = new ItemStack(Material.CARROT);
				ItemMeta itemmeta = item.getItemMeta();
				itemmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&fCenoura_PACK"));
				ArrayList<String> lore = new ArrayList<String>();
				lore.add(ChatColor.translateAlternateColorCodes('&', "&bClick na cenoura e Compre un Pack"));
				itemmeta.setLore(lore);
				item.setItemMeta(itemmeta);
				storefree.setItem(1, item);

				p.openInventory(storefree);
				p.playSound(p.getLocation(), Sound.BLOCK_BEACON_ACTIVATE, 0.5f, 0.5f);

			} else if (args.length == 1 && args[0].equalsIgnoreCase("VIP")) {

				ItemStack item = new ItemStack(Material.CARROT);
				ItemMeta itemmeta = item.getItemMeta();
				itemmeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&fCenoura_PACK"));
				ArrayList<String> lore = new ArrayList<String>();
				lore.add(ChatColor.translateAlternateColorCodes('&', "&bClick na cenoura e Compre un Pack"));
				itemmeta.setLore(lore);
				item.setItemMeta(itemmeta);
				storevip.setItem(1, item);

				p.openInventory(storevip);
				p.playSound(p.getLocation(), Sound.BLOCK_BEACON_ACTIVATE, 0.5f, 0.5f);
			}
		}

		if (cmd.getName().equalsIgnoreCase("lixeira")) {
			p.openInventory(trash);
			p.playSound(p.getLocation(), Sound.BLOCK_BEACON_ACTIVATE, 0.5f, 0.5f);
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void OnClick(InventoryClickEvent eventclick) {

		if (eventclick.getWhoClicked() instanceof Player) {
			if (eventclick.getView().getTitle().equals(ChatColor.translateAlternateColorCodes('&', "&bLoja FREE"))) {
				eventclick.setCancelled(true);
				Player p = (Player) eventclick.getWhoClicked();
				Inventory inv = p.getInventory();
				if (eventclick.getCurrentItem().getItemMeta().getDisplayName()
						.equals(ChatColor.translateAlternateColorCodes('&', "&fCenoura_PACK")) && money >= 0) {
					inv.addItem(new ItemStack(Material.CARROT, 64));
					p.closeInventory();
				}
			}
		}
	}

	private static Economy getEconomy() {
		return Main.econ;
	}

}
